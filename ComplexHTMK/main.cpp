#include <iostream>
#include <stdio.h>
#include <cstdlib>
#include <ctime>
#include <cmath>
#include <random>

using namespace std;

long double fn(long double, long double, int);
long double Expd(long double, long double);
long double Nc(long double);
long double A(long double);
long double f_n;
const int M = 43;
long double theta[M], theta0[M];
long double z[M];


//const long double kappa1 = 0.5, kappa2 = 0.1, k1 = 0.01, k2 = 0.0005, sigma = 0.00000000000567, T_max = 773;
//const long double Nc1 = (kappa1 * k1) / (4 * sigma * powl(T_max,3) * powl(n2,2)), Nc2 = (kappa2 * k2) / (4 * sigma * powl(T_max,3) * powl(n3,2));
const long double n1 = 1.5, n2 = 1.; //, n3 = 3.3;
const long double c1 = 0.2, c2 = 0.5;
const long double Nc1 = 0.1, Nc2 = 0.01;
const long double kappa1 = Nc1 * powl(n1, 2), kappa2 = Nc2 * powl(n2, 2);
const long double alpha_ = 0.001;
const long double theta1 = 1.;
const long double theta2 = 0.5;
const long double z0 = 0., z1 = 1., z2 = 2.;

int main(){
    
    time_t starttime, endtime;
    time ( &starttime );
    
    int N = 2000, n = 5, K = 50;
 // int iter = 0;
 // long double norm = 1.;
    long double nu, zz;
    long double ThetaFunc = 0., C1 = 0.;
    long double IntensiveN = 0., IntensiveK = 0.;
    
    z[0] = z0;
    z[1] = 0.05;
    z[2] = 0.1;
    z[M - 2] = 1.95;
    z[M - 1] = 2.;
    theta0[0] = theta1;
    theta0[M - 1] = theta2;
    theta[0] = theta1;
    theta[M - 1] = theta2;
    
    
    FILE *file;
    file = fopen("/Users/alenaastrahanceva/Documents/studies/disser/projects/ComplexHTMK/ComplexHTMK/data.dat","w+");
    
//    mt19937 rand((unsigned)time(NULL));
    srand((unsigned)time(NULL));
    for (int i = 2; i < 19; i++){
        z[i + 1] = z[i] + 0.05;
    }
    
    z[20] = 0.99;
    z[21] = 1.01;
    z[22] = 1.02;
    z[23] = 1.05;
    z[24] = 1.1;
    
    for (int i = 24; i < 40; i++){
        z[i + 1] = z[i] + 0.05;
    }
    for (int i = 0; i < (M - 2); i++){
        theta0[i + 1] = theta1 - 0.25 * z[i + 1];
    }
//  for (int i = 0; i < M; i++){
//      cout << "\n theta0 = " << theta0[i] << " for z = " << z[i] << " " << i;
//   }
    
//  while (norm > 0.00001){
    long double Intensive = 0.;
        for (int m = 1; m < K; m++){
       // cout << m << "\n";
            /////////////////
            if (m > K - 2){
            for (int j = 1; j <= N; j++){
                long double  alpha = (long double)rand() / RAND_MAX;
                zz = 0.999;
                nu = 2. * alpha - 1.;
                IntensiveK = (IntensiveK * (j - 1) + (fn(zz, nu, n) * nu)) / j;
               long double zz1 = 1.001;
               Intensive = (Intensive * (j - 1) + (fn(zz1, nu, n) * nu)) / j;
            }
                cout << IntensiveK << ", " << Intensive;
            }
            ////////////////
        for (int j = 1; j <= N; j++){
            long double  alpha = (long double)rand() / RAND_MAX;
            zz = z2 * alpha;
            alpha = (long double)rand() / RAND_MAX;
            nu = 2. * alpha - 1.;
            IntensiveK = (IntensiveK * (j - 1) + (fn(zz, nu, n) * nu * Nc(zz))) / j;
        }
        C1 = (- IntensiveK * z2  + theta2 - theta1) / (z1 / kappa1 + (z2 - z1) / kappa2);
        
        for (int i = 1; i < (M - 1); i++){
            for (int j = 1; j <= N; j++){
                long double alpha = (long double)rand() / RAND_MAX;
                zz = z[i] * alpha;
                alpha = (long double)rand() / RAND_MAX;
                nu = 2. * alpha - 1.;
                IntensiveN = (IntensiveN * (j - 1) + (fn(zz, nu, n) * nu / Nc(zz))) / j;
            }
            ThetaFunc = z[i] * IntensiveN + A(z[i]) * C1 + theta1;
            theta[i] = (1. - alpha_) * theta0[i] + alpha_ * ThetaFunc;
        }
        
//        long double accum = 0., sum = 0.;
//        
//        for (int m = 0; m < M; m++) {
//            accum = theta[m] - theta0[m];
//            sum += accum * accum;
//        }
//        norm = sqrt(sum);
//        iter++;
        //cout << "\n " << norm << " , " << iter;
        
        for (int i = 0; i < M; i++)
            theta0[i] = theta[i];
        
    }
    
//    for (int i = 0; i < M; i++){
//        cout << "\n theta = " << theta[i] << " for z = " << z[i];
//        fprintf(file,"%Lf %Lf\n", z[i], theta[i]);
//    }
    
    time ( &endtime);
//  cout << "\n time = " << endtime - starttime;
    
    return 0;
}

long double A(long double z){
    
    long double A = z / kappa1;
    
    if (z >= z1){
        A = z1 / kappa1 + (z - z1) / kappa2;
    }
    
    return A;
}

long double Expd(long double argz, long double argnu){
    
    long double argd = 0.;
    long double expd = 0.;
    
    if (abs(argnu) > 0.0000000000001){
        if (argz < z1){
            if (argnu > 0.){
                argd = argz / argnu;
            }
            else{
                argd = - (z1 - argz) / argnu;
            }
        }
        else if (argz == z1){
            if (argnu > 0.){
                argd = argz / argnu;
            }
            else{
                argd = - (z2 - argz) / argnu;
            }
        }
        else if (z1 < argz){
            if (argnu > 0.){
                argd = (argz - z1) / argnu;
            }
            else{
                argd = - (z2 - argz) / argnu;
            }
        }
        expd = expl(- argd);
    }
    
    return expd;
}

long double zi(long double argnu, long double argz){
    
    long double arz;
    
    if (argz < z1){
        if (argnu > 0.)
            arz = z0;
        else
            arz = z1;
    }
    else if (argz == z1){
        if (argnu > 0.)
            arz = z0;
        else
            arz = z2;
    }
    else if (z1 < argz){
        if (argnu > 0.)
            arz = z1;
        else
            arz = z2;
    }
    
    return arz;
}

long double h(long double xi){
    
    long double argh = 0.;
    
    if (xi == z0)
        argh = powl(theta1, 4);
    else if (xi == z2)
        argh = powl(theta2, 4);
    
    return argh;
}

long double n_tilda(long double nu, long double n1, long double n2){
    
    long double n = 0.;
    
    if (nu <= 0.)
        n = n1 / n2;
    else
        n = n2 / n1;
    
    return n;
}

long double psi(long double nu, long double n1, long double n2){
    
    long double argpsi = 0.;
    
    long double pow_ = (1. - powl(n_tilda(nu, n1, n2), 2) * (1. - powl(nu, 2)));
    if (pow_ > 0.){
        if (nu < 0.)
            argpsi = - sqrtl(pow_);
        else if (nu > 0.)
            argpsi = sqrtl(pow_);
    }
//  cout << "\n " << argpsi;
    return argpsi;
}

long double R(long double nu, long double n1, long double n2){
    
    long double argR = 1.;
    
    long double psi_ = psi(nu, n1, n2);
    long double n_tilda_ = n_tilda(nu, n1, n2);
    long double pow_ = (1. - powl(n_tilda_, 2) * (1. - powl(nu, 2)));
    if (pow_ > 0.)
        argR = 0.5 * (powl((psi_ * n_tilda_  - nu) / (psi_ * n_tilda_  + nu), 2) + powl((psi_ - n_tilda_  * nu) / (psi_ + n_tilda_  * nu), 2));
//  cout << "\n " << argR;
    return argR;
}

long double ThetaBet(long double zz){
    
    long double thetaarg = 0.;
    
    for (int i = 0; i < (M - 1); i++){
        if ((z[i] <= zz) && (zz < z[i + 1])){
            thetaarg = theta0[i] + ((theta0[i + 1] - theta0[i]) / (z[i + 1] - z[i])) * (zz - z[i]);
        }
    }
    
    return thetaarg;
}

long double Nc(long double argz){
    
    long double nc;
    
    if (argz <= z1)
        nc = Nc1;
    else
        nc = Nc2;
    
    return nc;
}

long double c(long double argz){
    
    long double c;
    
    if (argz <= z1)
        c = c1;
    else
        c = c2;
    
    return c;
}


long double fn(long double zz, long double nu,  int n){
    
    long double Npsi, NR, f0, z_old, z_, nu_old, Expd_;
  
    z_old = zz;
    nu_old = nu;
    Expd_ = Expd(z_old, nu_old);
    z_ = zi(nu,zz);
    
    if (n > 1){
    
        long double  alpha = (long double)rand()/RAND_MAX;
        long double  tau =  - logl (1. - alpha * (1. - Expd_));
        zz = zz - tau * nu;
        alpha = (long double)rand()/RAND_MAX;
        nu = 2. * alpha - 1.;
        
        if (z_ == z0){
            NR = 0.7;
//          NR = R(nu_old, n1, n3);
            f_n = (NR * fn(z_, - nu_old, n - 1) + (1. - NR) * h(z_)) * Expd_  + (1. - Expd_) * (c(zz) * fn(zz, nu, n - 1)  +  (1. - c(zz)) * powl(ThetaBet(zz), 4));
        }
        else if (z_ == z1){
            NR = R(nu_old, n1, n2);
            Npsi = psi(nu_old, n1, n2);
//          cout << "\n " << Npsi;
            if (Npsi == 0.){
                NR = 1.;
                f_n = (NR * fn(z_, - nu_old, n - 1)) * Expd_ + (1. - Expd_) * (c(zz) * fn(zz, nu, n - 1) + (1. - c(zz)) * powl(ThetaBet(zz),4));
            }
            else
                f_n = (NR * fn(z_, - nu_old, n - 1) + (1. - NR) * fn(z_, Npsi, n - 1)) * Expd_ + (1. - Expd_) * (c(zz) * fn(zz, nu, n - 1) + (1. - c(zz)) * powl(ThetaBet(zz),4));
        }
        else if (z_ == z2){
            NR = 0.7;
//          NR = R(nu_old, n2, n3);
            f_n = (NR * fn(z_, - nu_old, n - 1) + (1. - NR) * h(z_)) * Expd_ + (1. - Expd_) * (c(zz) * fn(zz, nu, n - 1) + (1. - c(zz)) * powl(ThetaBet(zz),4));
        }
        
        return f_n;
    }
    else{
        
        long double  alpha = (long double)rand()/RAND_MAX;
        long double  tau =  - logl (1. - alpha * (1. - Expd_));
        zz = zz - tau * nu;
        
        if (z_ == z0)
//          NR = R(nu, n1, n3);
            NR = 0.7;
        else if (z_ == z1)
            NR = 1.;
        else if (z_ == z2)
            NR = 0.7;
//          NR = R(nu, n2, n3);
        
        f0 = Expd_ * h(z_) * (1. - NR) + (1. - c(zz)) * (1. - Expd_) * powl(ThetaBet(zz),4);
        
        return f0;
    }
}


